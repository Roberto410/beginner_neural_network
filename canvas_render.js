// Create Canvas
var canvas = document.createElement("canvas");
canvas.width = 900;
canvas.height = 900;
document.body.appendChild(canvas);
var ctx = canvas.getContext("2d");
ctx.font = "Arial";

var graph_size = {width: 7, height: 7};

var params = train();

// visualize model output
ctx.clearRect(0, 0, canvas.width, canvas.height);
draw_grid();
draw_points();
visualize_params(params);

// say what the model would say for a given mouse position
window.onmousemove = function(evt) {
  ctx.clearRect(0, 0, 100, 50);

  let p = {x: 10, y: 20};

  let mouse = {x: evt.offsetX, y: evt.offsetY};
  let mouse_graph = to_graph(mouse.x, mouse.y);

  ctx.fillText("x: " + Math.round(mouse_graph.x*100)/100, p.x, p.y);
  ctx.fillText("y: " + Math.round(mouse_graph.y*100)/100, p.x, p.y + 10);
  // model output
  let model_out = sigmoid( mouse_graph.x * params.w1 + mouse_graph.y * params.w2 + params.b );
  model_out = Math.round(model_out*100)/100;
  ctx.fillText("prediction: " + model_out, p.x, p.y + 20);
}
