/*
* Author: Robert D Clark
*
* Adpted from www.youtube.com/watch?v=gQLKufQ35VE&t=1207s - Beginner Intro to Neural Networks 11: Neural Network in Javascrip - by: giant_neural_network
*
*/


// Unknown type (data we want to find)
var dataU = [5.3,  6.5, "it should be 1"];


// Known Data Points. 
// Training set. [length, width, color(0=blue and 1=red)]
var all_points = [

    // Blue Flowers
    [4, 6, 0], 
    [3, 6, 0],
    [2, 4, 0],
    [4, 5, 0],
    [4, 6, 0],
    [3, 5, 0],
    [3, 4, 0],

    // Red Flowers
    [5, 2, 1],
    [5, 3, 1],
    [4, 2, 1],
    [5, 1, 1],
    [2, 2, 1],
    [2, 4, 1],
    [4, 3, 1],
    [5, 3, 1],
    [3, 2.5, 1],
    [4, 2.5, 1],
    [6, 3, 1],
    [5, 4, 1]
];

var numberOfIterations;

// Defines sigmoid normalisation function
function sigmoid(x) {
  return 1/(1+Math.exp(-x));
}

// Training function
function train() {
  let w1 = Math.random()*.2-.1;
  let w2 = Math.random()*.2-.1;
  let b = Math.random()*.2-.1;
  let learning_rate = 0.2;
  numberOfIterations = parseInt(document.getElementById('number_of_iterations').value)
  for (let iter = 0; iter < numberOfIterations; iter++) {
    // pick a random point
    let random_idx = Math.floor(Math.random() * all_points.length);
    let point = all_points[random_idx];
    let target = point[2]; // target stored in 3rd coord of points

    // feed forward
    let z = w1 * point[0] + w2 * point[1] + b;
    let pred = sigmoid(z);

    // now we compare the model prediction with the target
    let cost = (pred - target) ** 2;

    // now we find the slope of the cost w.r.t. each parameter (w1, w2, b)
    // bring derivative through square function
    let dcost_dpred = 2 * (pred - target);

    // bring derivative through sigmoid
    // derivative of sigmoid can be written using more sigmoids! d/dz sigmoid(z) = sigmoid(z)*(1-sigmoid(z))
    let dpred_dz = sigmoid(z) * (1-sigmoid(z));

    // I think you forgot these in your slope calculation? 
    let dz_dw1 = point[0];
    let dz_dw2 = point[1];
    let dz_db = 1;

    // now we can get the partial derivatives using the chain rule
    // notice the pattern? We're bringing how the cost changes through each function, first through the square, then through the sigmoid
    // and finally whatever is multiplying our parameter of interest becomes the last part
    let dcost_dw1 = dcost_dpred * dpred_dz * dz_dw1;
    let dcost_dw2 = dcost_dpred * dpred_dz * dz_dw2;
    let dcost_db =  dcost_dpred * dpred_dz * dz_db;

    // now we update our parameters!
    w1 -= learning_rate * dcost_dw1;
    w2 -= learning_rate * dcost_dw2;
    b -= learning_rate * dcost_db;

    //Write the values to the console
  }

  return {w1: w1, w2: w2, b: b};
}
